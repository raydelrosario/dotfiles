echo "Installing submodules"
git submodule update --init --recursive
echo "Dotbot"
./dotbot/bin/dotbot -c install.conf.yaml
echo "Dotbot Paru"
./dotbot/bin/dotbot -p dotbot-paru/paru.py -c packages.conf.yaml
echo "Install Vim Plug"
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
echo "Install Ranger Deviceicons"
git clone https://github.com/alexanderjeurissen/ranger_devicons ~/.config/ranger/plugins/ranger_devicons
